import React from 'react';
import PropTypes from "prop-types";


const Item = React.memo(function Item({id, title, completed, onDelete, onComplete, setEdited, setTitle}) {

    const handleOnDelete=()=>{
        onDelete(id)
    }

    const handleOnComplete=()=>{
        onComplete(id)
    }

    const handleOnClickEdit=()=>{
        setEdited(id)
        setTitle(title)
    }
    const handleOnDoubleClickEdit=()=>{
        setEdited(0)
        setTitle('')
    }


    return (
        <li className="list-group-item">
            <div className="todo-indicator bg-warning"/>
            <div className="widget-content p-0">
                <div className="widget-content-wrapper">
                    <div className="widget-content-left mr-2">
                        <div className="custom-control custom-checkbox">
                            <input type="checkbox" className="custom-control-input" id={id} onChange={handleOnComplete}/>
                            <label className="custom-control-label" htmlFor={id}/>
                        </div>

                    </div>
                    <div className="widget-content-left">
                        {completed?<div className={completed?"widget-heading todo-completed":"widget-heading"}>{title} <div
                                className={"badge badge-success ml-2"}>Done</div>
                            </div>:
                            <div className={completed?"widget-heading todo-completed":"widget-heading"}>{title} <div
                                className={"badge badge-danger ml-2"}>Left</div>
                            </div>
                        }
                    </div>
                    <div className="widget-content-right">
                        <button
                            className="border-0 btn-transition btn btn-outline-dark" onClick={handleOnClickEdit} onDoubleClick={handleOnDoubleClickEdit}>
                            <i className="fa fa-pencil-square-o"/></button>
                        <button
                            className="border-0 btn-transition btn btn-outline-danger" onClick={handleOnDelete}>
                            <i className="fa fa-trash"/></button>
                    </div>
                </div>
            </div>
        </li>
    );
})

Item.propTypes = {
    id:PropTypes.number.isRequired,
    title:PropTypes.string.isRequired,
    completed:PropTypes.bool.isRequired,
    onDelete:PropTypes.func.isRequired,
    onComplete:PropTypes.func.isRequired,
    setEdited:PropTypes.func.isRequired,
    setTitle:PropTypes.func.isRequired
}

Item.propDefaultsTypes = {
    id:0,
    title:'',
    completed:false,
    onDelete:null,
    onComplete:null,
    setEdited:null,
    setTitle:null
}

export default Item;
