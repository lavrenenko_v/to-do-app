import React, {useEffect, useRef, useState} from "react";
import PropTypes from "prop-types";

const Sort = React.memo(function Sort({activeSortType, items, onClickSortType}){
    const [visiblePopup, setVisiblePopup] = useState(false)
    const sortRef = useRef()
    const getName = (type)=>{
        return type!==null?items[items.findIndex(obj=>obj.type===type)].name:'All'
    }
    const onSelectItem = (type)=>{
        onClickSortType(type)
        setVisiblePopup(false)
    }

    useEffect(()=>{
        document.body.addEventListener('click', handleOutSideClick)
        //  console.log(sortRef.current)
    },[])

    const handleOutSideClick = (event)=>{
        const path = event.path || (event.composedPath && event.composedPath())
        if(!path.includes(sortRef.current)){
            setVisiblePopup(false)
        }
    }

    const toggleVisiblePopup = ()=>{
        setVisiblePopup(!visiblePopup)
    }

    return (
        <div
            className="dropdown">
            <button onClick={()=>toggleVisiblePopup()} className="btn btn-secondary dropdown-toggle btn-sm" type="button" id="dropdownMenuButton"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {getName(activeSortType)}
            </button>
            {!visiblePopup && <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                {
                    items && items.map((item, index)=>(
                        <button key={index} className={activeSortType===item.type?'dropdown-item active': 'dropdown-item' } onClick={()=>onSelectItem(item.type)}>{item.name}</button>
                    ))
                }
            </div>}
        </div>
    )
})

Sort.propTypes = {
    activeSortType:PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
    items:PropTypes.arrayOf(PropTypes.object),
    onClickSortType:PropTypes.func.isRequired
}

Sort.propDefaultsTypes = {
    activeSortType:null,
    items:[],
    onClickSortType:null
}
export default Sort;
