import React from "react";
import PropTypes from "prop-types";


const Form = ({handleSubmit, onChange, register, edited, editedTitle})=>{

    return (
        <form onSubmit={handleSubmit(onChange)} className="form-inline">
            <div className="form-group">
                <input {...register("title")} placeholder={editedTitle?editedTitle:'Add task'} id='inputTitle'/>
                <input type="submit" className="btn btn-primary" value={edited===0?'Add':'Edit'}/>
            </div>
        </form>
    )
}

Form.propTypes={
    handleSubmit: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    edited:PropTypes.number.isRequired,
    editedTitle:PropTypes.string
}

Form.propDefaultsTypes={
    handleSubmit: null,
    onChange: null,
    edited: 0,
    editedTitle: ''
}

export default Form;
