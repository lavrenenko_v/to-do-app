import {createStore, compose, applyMiddleware} from 'redux'
import rootStoreReducer from "./reducers";
import thunk from 'redux-thunk'


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootStoreReducer, composeEnhancers(applyMiddleware(thunk)));

window.store = store;

export default store;
