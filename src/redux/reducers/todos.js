const initialState = {
    items:[
        {id:1, title:'Sharpen English speaking skills', completed:false},
        {id:2, title:'Wrap up the Todos project', completed: false},
    ],
    totalCount:2
}

const getCount = (items)=>{
    return items?items.filter(item => item.completed!==true).length:0;
}

const todosReducer = (state=initialState, action)=>{
    switch (action.type){
        case 'ADD_TODO':{
            const newItems = [...state.items, action.payload]
            return {
                ...state,
                items:newItems,
                totalCount: getCount(newItems)
            }
        }
        case 'REMOVE_TODO_ITEM':{
            const newItems = [...state.items]
            newItems.splice(newItems.findIndex(item => item.id === action.payload), 1)
            return {
                ...state,
                items:newItems,
                totalCount: getCount(newItems)
            }
        }
        case 'CLEAR_TODOS':{
            return {
                ...state,
                items:[],
                totalCount: 0
            }
        }
        case 'COMPLETE_TO_DO_ITEM':{
            const newItems = [...state.items]
            const objIndex = newItems.findIndex((obj => obj.id === action.payload));
            newItems[objIndex].completed = !newItems[objIndex].completed

            return {
                ...state,
                items:newItems,
                totalCount:getCount(newItems)
            }
        }
        case 'EDIT_TO_DO_ITEM':{
            const {id, title} = action.payload
            const newItems = [...state.items]
            const objIndex = newItems.findIndex((obj => obj.id === id))
            newItems[objIndex].title = title

            return {
                ...state,
                items:newItems
            }
        }
        default:{
            return state
        }
    }
}

export default todosReducer;
