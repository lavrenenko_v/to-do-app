export const addToDoItem = (todoObj)=>({
    type: 'ADD_TODO',
    payload:todoObj
})

export const removeToDoItem = (id)=>({
    type: 'REMOVE_TODO_ITEM',
    payload: id
})

export const clearAllToDos = ()=>({
    type: 'CLEAR_TODOS'
})

export const completeToDoItem = (id)=>({
    type: 'COMPLETE_TO_DO_ITEM',
    payload: id
})


export const editToDoItem = (obj)=>({
    type: 'EDIT_TO_DO_ITEM',
    payload:obj
})
