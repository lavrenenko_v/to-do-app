import {useDispatch, useSelector} from "react-redux";
import Item from "../components/Item";
import React, {useState} from "react";
import {Form, Sort} from "../components";
import {addToDoItem, completeToDoItem, editToDoItem, removeToDoItem} from "../redux/actions/todos";
import {useForm} from "react-hook-form"
import {setSortBy} from "../redux/actions/filters";


const sortItems = [
    { name: 'All', type:null},
    { name: 'Done', type:true },
    { name: 'Left', type: false},
];


const Home=()=>{
    const {register, handleSubmit} = useForm()
    const dispatch = useDispatch()
    const {items, totalCount} = useSelector(({todos})=>todos)
    const {sortBy} = useSelector(({filters})=>filters)
    const [edited, setEdited] = useState(0)
    const [editedTitle, setEditedTitle] = useState('')


    const getRandomId = ()=>{
        let [min, max] = [100, 1000]
        return Math.floor(Math.random() * (max - min) + min);
    }

    const clearInput = ()=>{
        document.getElementById('inputTitle').value = ''
    }

    const onDeleteItem = (id)=>{
        dispatch(removeToDoItem(id))
    }

    const onCompleteItem = (id)=>{
        dispatch(completeToDoItem(id))
    }

    const onEditItem = (id, title)=>{
        dispatch(editToDoItem({id, title}))
    }

    const onChangeItem = (d)=>{
        if (!d.title) {
            alert('You must add the title')
            return
        }
        if(edited!==0){
            const obj = {
                id:edited,
                title:d.title
            }
            clearInput()
            setEdited(0)
            setEditedTitle('')
            dispatch(editToDoItem(obj))
        } else {
            setEdited(0)
            const obj = {
                id: getRandomId(),
                title: d.title,
                completed: false
            }
            clearInput()
            setEditedTitle('')
            dispatch(addToDoItem(obj))
        }
    }

    const onClickSortType = (type)=>{
        dispatch(setSortBy(type))
    }

    return (
        <>
            <div className="row d-flex justify-content-center container">
                <div className="col-md-8">
                    <div className="card-hover-shadow-2x mb-3 card">
                        <div className="card-header-tab card-header">
                            <div className="card-header-title font-size-lg text-capitalize font-weight-normal"><i
                                className="fa fa-tasks"/>&nbsp;Tasks: {totalCount}
                            </div>
                            <div className='card-body text-right'>
                                <Sort items={sortItems} activeSortType={sortBy} onClickSortType={onClickSortType}/>
                            </div>
                        </div>


                        <div className="scroll-area-sm">
                            <perfect-scrollbar className="ps-show-limits">
                                <div styles="position: static;" className="ps ps--active-y">
                                    <div className="ps-content">
                                        <ul className=" list-group list-group-flush">
                                            {
                                                items&&(sortBy===null?
                                                    items.map((todo, index)=>(
                                                        <Item key={index} id={todo.id} title={todo.title} completed={todo.completed} onDelete={onDeleteItem} onComplete={onCompleteItem} onEdit={onEditItem} onChange={onChangeItem} setEdited={setEdited} setTitle={setEditedTitle}/>
                                                    )):items.filter(obj=>obj.completed===sortBy).map((todo, index)=>(
                                                        <Item key={index} id={todo.id} title={todo.title} completed={todo.completed} onDelete={onDeleteItem} onComplete={onCompleteItem} onEdit={onEditItem} onChange={onChangeItem} setEdited={setEdited} setTitle={setEditedTitle}/>
                                                    )))
                                            }
                                        </ul>
                                    </div>
                                </div>
                            </perfect-scrollbar>
                        </div>

                        <div className="d-block text-center card-footer d-flex justify-content-center align-items-center container">
                            <Form handleSubmit={handleSubmit} onChange={onChangeItem} register={register} edited={edited} editedTitle={editedTitle}/>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Home;
